# miniconda-rocker-geospatial-gmtsar

## miniconda-geospatial

This container is based on [continuumio/miniconda](https://hub.docker.com/r/continuumio/miniconda/) and it includes the `shapely`, `rasterio` and `fiona` packages. [Miniconda](http://conda.pydata.org/miniconda.html) (based on Python 2.7) is ready to use. 

The Miniconda distribution is installed into the `/opt/conda` folder and ensures that the default user has the `conda` command in their path.

### Usage

You can run this image using the following commands:

    docker run -i -t atavares/miniconda-rocker-geospatial-gmtsar /bin/bash

Alternatively, you can start a Jupyter Notebook server and interact with Miniconda via your browser:

    docker run -i -t -p 8888:8888 atavares/miniconda-rocker-geospatial-gmtsar /bin/bash -c "/opt/conda/bin/jupyter notebook --notebook-dir=/opt/notebooks --ip='*' --port=8888 --no-browser --allow-root"

You can then view the Jupyter Notebook by opening `http://localhost:8888` in your browser, or `http://<DOCKER-MACHINE-IP>:8888` if you are using a Docker Machine VM.

## rocker-geospatial

This image is based on [rocker/geospatial](https://hub.docker.com/r/rocker/geospatial/), a Docker-based Geospatial toolkit for R, built on versioned Rocker images.

### Usage

You can run RStudio using the following command:

    docker run -d -p 8787:8787 atavares/miniconda-rocker-geospatial-gmtsar

You can then open RStudio with  `http://localhost:8787`, or `http://<DOCKER-MACHINE-IP>:8787` if you are using a Docker Machine VM.

Check out more options on [this page](https://github.com/rocker-org/rocker/wiki/Using-the-RStudio-image).


## GMTSAR

GMTSAR is an open source (GNU General Public License) InSAR processing system. Check [this page](http://topex.ucsd.edu/gmtsar/) for more info.


### Usage

You can run this image from the shell, use the following command:

    docker run -i -t atavares/miniconda-rocker-geospatial-gmtsar /bin/bash




